# Generated by Django 2.0.2 on 2018-02-06 17:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('exchange_payments', '0006_auto_20180125_2132'),
    ]

    operations = [
        migrations.AlterField(
            model_name='currencygateway',
            name='gateway',
            field=models.CharField(choices=[('coinpayments', 'CoinPayments'), ('tcoin', 'Tcoin')], max_length=50),
        ),
    ]
